import React, { Component } from 'react';
import {
	Badge, Button,
	Row, Col,
	Card, CardHeader, CardBody, CardFooter,
	Modal, ModalHeader, ModalBody, ModalFooter,
	Pagination, PaginationItem, PaginationLink,
	Table, Collapse,
	FormGroup, Input, Label,
} from 'reactstrap';

export class Billing extends Component {
	constructor(props) {
		super(props);
		this.state = {
			collapse: false,
			status: 'Closed',
			modalOpened: false,
		};
		this.rows = [
			{ id: 1, name: 'Vishnu Serghei', date: '2012/01/01', type: 'Member', status: 'Active', colorStatus: 'success' },
			{ id: 2, name: 'Zbyněk Phoibos', date: '2012/02/01', type: 'Staff', status: 'Banned', colorStatus: 'danger' },
			{ id: 3, name: 'Einar Randall', date: '2012/02/01', type: 'Admin', status: 'Inactive', colorStatus: 'secondary', },
			{ id: 4, name: 'Félix Troels', date: '2012 / 03 / 01', type: 'Member', status: 'Pending', colorStatus: 'warning', },
			{ id: 5, name: 'Aulus Agmundr', date: '2012 / 01 / 21', type: 'Staff', status: 'Active', colorStatus: 'success', },
		];
	}

	toggleCollapse() {
		this.setState({ collapse: !this.state.collapse });
	}
	toggleModal() {
		this.setState({ modalOpened: !this.state.modalOpened });
	}

	selectRow(row, mobile) {
		const route = this.getRowRoute(row, mobile);
		this.props.history.push(route);
	}
	getRowRoute(row, mobile) {
		return `/billing${mobile ? 'mobile' : ''}/${row.id}`;
	}
	renderRow(row, idx) {
		const mobile = true;
		const route = this.getRowRoute(row);
		return (
			<tr
				key={row.id || idx}
			>
				<td>{row.name}</td>
				<td>{row.date}</td>
				<td className="text-center">
					<a href={'/#' + this.getRowRoute(row)}>
						<Badge
							className="cursor-pointer"
							onClick={() => this.selectRow(row)}
						>Desktop</Badge>
					</a>
				</td>
				<td className="text-center">
					<a href={'/#' + this.getRowRoute(row, mobile)}>
						<Badge
							className="cursor-pointer"
							onClick={() => this.selectRow(row, mobile)}
						>Mobile</Badge>
					</a>
				</td>
			</tr>
		);
	}
	render() {
		const colSizes = { md: 4, sm: 6, xs: 12, };
		return (
			<div className="animated fadeIn">
				<Row>
					<Col>
						<Card>
							<CardHeader>
								<i className="fa fa-align-justify"></i> Lista de facturas
            				</CardHeader>
							<CardBody>
								<Card>
									<CardHeader>
										<div className="" onClick={() => this.toggleCollapse()}>
											<i className="fa fa-search"></i> <strong>Filtros</strong>
										</div>

										<div className="card-actions">
											<Button className="pd-h-5" color="success" onClick={() => this.toggleModal()}>Agregar</Button>
										</div>
									</CardHeader>
									<Collapse isOpen={this.state.collapse}
									>
										<CardBody>
											<Row>
												<Col {...colSizes}>
													<FormGroup>
														<Label className="mg-nn" htmlFor="name">Name</Label>
														<Input type="text" id="name" placeholder="Enter your name" required />
													</FormGroup>
												</Col>
												<Col {...colSizes}>
													<FormGroup>
														<Label className="mg-nn" htmlFor="name">Name</Label>
														<Input type="text" id="name" placeholder="Enter your name" required />
													</FormGroup>
												</Col>
												<Col {...colSizes}>
													<FormGroup>
														<Label className="mg-nn" htmlFor="name">Name</Label>
														<Input type="text" id="name" placeholder="Enter your name" required />
													</FormGroup>
												</Col>
											</Row>
										</CardBody>
										<CardFooter>
											<Button color="success" onClick={() => this.toggleModal()}>Buscar</Button>
											<Button color="info" onClick={() => this.toggleModal()}>Limpiar</Button>
										</CardFooter>
									</Collapse>
								</Card>
								<Table hover bordered striped responsive size="sm">
									<thead>
										<tr>
											<th>Username</th>
											<th>Date registered</th>
											<th colSpan="2">Open detail as version</th>
										</tr>
									</thead>
									<tbody>
										{this.rows.map(row => this.renderRow(row))}
									</tbody>
								</Table>
								<nav>
									<Pagination>
										<PaginationItem>
											<PaginationLink previous href="#">Anterior</PaginationLink>
										</PaginationItem>
										<PaginationItem active>
											<PaginationLink href="#">1</PaginationLink>
										</PaginationItem>
										<PaginationItem><PaginationLink href="#">2</PaginationLink></PaginationItem>
										<PaginationItem><PaginationLink href="#">3</PaginationLink></PaginationItem>
										<PaginationItem><PaginationLink href="#">4</PaginationLink></PaginationItem>
										<PaginationItem><PaginationLink next href="#">Siguiente</PaginationLink></PaginationItem>
									</Pagination>
								</nav>
							</CardBody>
						</Card>
					</Col>
				</Row>
				<Modal isOpen={this.state.modalOpened} toggle={() => this.toggleModal()}>
					<ModalHeader toggle={() => this.toggleModal()}>Modal title</ModalHeader>
					<ModalBody>
						<FormGroup>
							<Label className="mg-nn" htmlFor="name">Name</Label>
							<Input type="text" id="name" placeholder="Enter your name" required />
						</FormGroup>
						<FormGroup>
							<Label className="mg-nn" htmlFor="name">Name</Label>
							<Input type="text" id="name" placeholder="Enter your name" required />
						</FormGroup>
						<FormGroup>
							<Label className="mg-nn" htmlFor="name">Name</Label>
							<Input type="text" id="name" placeholder="Enter your name" required />
						</FormGroup>
						<FormGroup>
							<Label className="mg-nn" htmlFor="name">Name</Label>
							<Input type="text" id="name" placeholder="Enter your name" required />
						</FormGroup>
					</ModalBody>
					<ModalFooter>
						<Button className="mg-h-5" color="primary" onClick={() => this.toggleModal()}>
							Do Something
						</Button>
						<Button className="mg-h-5" color="secondary" onClick={() => this.toggleModal()}>
							Cancel
						</Button>
					</ModalFooter>
				</Modal>
			</div>
		)
	}
}

export default Billing;
