import React, { Component } from 'react';
import {
	Button,
	Modal, ModalHeader, ModalBody, ModalFooter,
	FormGroup, Input, Label,
} from 'reactstrap';

export class ProductModal extends Component {

	render() {
		const { toggle, opened, state } = this.props;
		return (
			<Modal isOpen={opened} toggle={toggle}>
				<ModalHeader toggle={toggle}>Producto</ModalHeader>
				<ModalBody>
					<FormGroup>
						<Label className="mg-nn" htmlFor="name">Name</Label>
						<Input type="text" id="name" placeholder="Enter your name" required />
					</FormGroup>
					<FormGroup>
						<Label className="mg-nn" htmlFor="name">Name</Label>
						<Input type="text" id="name" placeholder="Enter your name" required />
					</FormGroup>
					<FormGroup>
						<Label className="mg-nn" htmlFor="name">Name</Label>
						<Input type="text" id="name" placeholder="Enter your name" required />
					</FormGroup>
					<FormGroup>
						<Label className="mg-nn" htmlFor="name">Name</Label>
						<Input type="text" id="name" placeholder="Enter your name" required />
					</FormGroup>
				</ModalBody>
				<ModalFooter>
					<Button className="mg-h-5" color="primary" onClick={toggle}>
						Do Something
					</Button>
					<Button className="mg-h-5" color="danger" onClick={toggle}>
						Cancel
					</Button>
				</ModalFooter>
			</Modal>
		);
	}
}

export default ProductModal;
